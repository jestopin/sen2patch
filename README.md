# sen2patch

### sen2patch allows to gather spatial and environmental context data informing a list of geo-localised occurrences.

**End-product:** Image-series centered on each provided occurrence. Image size can be easily set, default is 640 x 640 m with one image per month for a year. Only the four spectral bands (RGB & IR) with 10 m spatial resolution are considered for now.

- Sentinel-2 imagery is exploited through the `sentinelsat` API: https://sentinelsat.readthedocs.io/en/stable/ 
- A Scihub Copernicus account is needed to query and download Sentinel-2 products. It can be quickly created at https://scihub.copernicus.eu/ but up to two days can be needed to be granted API access.

- To start using sen2patch scripts:
    - `git clone git@gitlab.inria.fr:jestopin/sen2patch.git` or https equivalent
    - `cd sen2patch`
    - `conda env create --file sen2env.yml`
    - `jupyter notebook` and start using the numerated notebooks and python scripts. 

- Method overview:

![sen2patch_Method_workflow_v2](/uploads/4f86516a795b26ecb0c785017abbd192/sen2patch_Method_workflow_v2.png)
