#!/usr/bin/env python
# coding: utf-8

# Reorganises patches stored in targeted_p folder with reference dataset_p by gbifid and moves them in gbif_dir.

# ## I. Imports and input data

# In[1]:
import numpy as np
import os
import pathlib
import shutil
import glob
import pandas as pd
from subprocess import Popen


# *** INPUT PATHS ***
targeted_p = "../data/patches/16bits_png/duplicates/"
occs_p  = "../data/occurrences/final_datasets/"
dataset_p  = "../data/occurrences/final_datasets/Dduplicates_default.npz"
gbifid_dir = "../data/patches/16bits_png/duplicates_by_gbifid"
# ****
# # change to ...
# targeted_p = "../data/patches/16bits_png/"
# dataset_p  = "../data/occurrences/final_datasets/Ddataset_default.npz"
# gbifid_dir = "../data/patches/16bits_png/final_dataset_by_gbifid"
# #               ... for final_dataset



# Checks if exists already and creates it if not
if not(os.path.exists(gbifid_dir)):
    os.mkdir(gbifid_dir)
    print("Folder created: ", gbifid_dir)


# - Tries to load them
# In[15]:

# dataset
data1     = np.load(dataset_p, allow_pickle=True)
Ddataset  = data1['D'].item()
print("len(Ddataset)\t\t:", len(Ddataset))

# Converts dataset dictionary to dataframe
dataset = pd.DataFrame.from_dict(Ddataset, orient='index').reset_index()
# rename index #
dataset.rename(columns={'index': 'id'}, inplace=True)

# A SAMPLE
#     # Select only first rows
# duplicates = duplicates.head(5) # 20 first for now
# print("duplicates.shape\t:", duplicates.shape)


# Saves corresponding csv
dataset.to_csv(os.path.join(occs_p, gbifid_dir.split('/')[-1]+".csv"), sep=';')


# ## IV. Moves duplicates files in another directory

# - Utility function
# In[17]:


def retrieves_row_files(row):
    # files = list of ABSOLUTE paths to the patches of the inputted occurrence row
    # Retrieves month prods list:
    prods = [idx for idx in row.index if 'prod' in idx]
    # List of files
    files = []
    # Loop on all months
    for p in prods:
        if not(pd.isna(row[p])):
            # Retrieves product's tile
            tile = row[p].split('_')[5][1:]
            # Retrieves product's UTM zone
            zone = tile[:2]
            # Retrieves products's month and year
            month, year = p.split('_')[1], p.split('_')[2]
            # file name
            names = '_'.join(['???', year, month, str(row['id']), '.png'])
            # ABSOLUTE paths
                # NOT ALL files with *  but specif month RGB & IR patches only
            path = os.path.join(targeted_p, zone, tile, str(row['id']), names)
            # Appends matching files
                # WITH SHUTIL
            files += glob.glob(path)
                # WITH POPEN
            # files += [path]

    return files


# In[18]:


# TEST
# A = duplicates.iloc[0]
# retrieves_row_files(A)


# - Function to reorganise files

# In[19]:
def moves_df_files(df, dest_dir):
    """Moves patches from df to dest_dir."""
    # files_series = series of lists. Each list (one per occ) contains the paths to the month patches to move
    df2 = df.assign(files_to_move = df.apply(lambda row: retrieves_row_files(row), axis=1))[["id", "files_to_move"]].set_index('id')

    # Loop on gbifids
    for i, idx in enumerate(df2.index):
        print(i, ':', idx)
        print("len(df2.files_to_move[idx]):", len(df2.files_to_move[idx]))
        # print(df2.files_to_move[idx])
        # print()

        id_      = str(idx)
        # folders that contain patches
        folder_1 = id_[-2:]
        folder_2 = id_[-4:-2]
        # path to patches
        path     = os.path.join(dest_dir, folder_1, folder_2, id_)
        # print("\tnew path:", path)
        # Creates or retrieves it
        pathlib.Path(path).mkdir(parents=True, exist_ok=True)
        # print('\tPath:', path)

        # COPIES/MOVES files, file name only not whole path
        for f in df2.files_to_move[idx]:
            try:
                # print("\tNew file:", f)
                # WITH SHUTIL
                shutil.move(f, path)
            except BlockingIOError:
                print("BlockingIOError for file:", f)
                # WITH CP
                Popen("mv "+f+' '+path, shell=True)

        # # WITH SHUTIL
        # [shutil.copy(f, path) for f in df2.files_to_move[idx]]

# In[20]:

## Moves duplicates files in another place
moves_df_files(dataset, gbifid_dir)
##

