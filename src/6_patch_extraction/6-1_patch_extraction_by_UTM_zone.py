#!/usr/bin/env python
# coding: utf-8

# In[3]:

import numpy as np
import os
import os.path
import glob
import time
from shapely.geometry import Point, MultiPolygon
import pandas as pd
import tempfile
import tarfile
import gc
from datetime import datetime
import pathlib
# Works with https://github.com/maximiliense/Datascience-Engine installed, otherwise work with argparse or other workaround
from ds_engine.memory import get_parameter

# In[9]:

# Utility functions imports
from coordinates_functions import *
from flags_functions import *
from products_functions import *
from patches_functions import *

print("Imports OK")
# In[100]:

# HARD CODED
# *** INPUT PATHS ***
extract_ref   = "../data/occurrences/extraction_ref_sample300.csv.csv"
dico_utm_p    = "../data/occurrences/D_utm.npz"
tiles_p       = "../data/products/by_tile/"
dest_p        = "../data/"
# User choices to retrieve temporal frequency
usr_choices_p = "../user_choices.npz"
######################
usr_choices   = np.load(usr_choices_p, allow_pickle=True)
sav_format    = data["sav_format"].item()
bands         = data["bands"].item()

# *** CONSTANTS ***********************************************************
N           = 10980 # Pixels dimension in a spectral band (width=height)
PATCH_SIZE  = 64    # Square patch size in pixels
PIXEL_RES_m = 10    # Field pixel size in meters
SEC_COEF    = 2     # Multiplying coef. when defining a margin around data products footprints
                    # margin =  PIXEL_RES_m x (PATCH_SIZE//2 * SEC_COEF)
# *************************************************************************


# ### INLINE Argument
    # argparse:
# parser = argparse.ArgumentParser(description='Let the user choose the UTM zone to be patch-extracted')
# parser.add_argument("-z", "--UTM_zone", help="Chosen UTM zone, must be an int in [1,60]", type=int)
# args = vars(parser.parse_args())
# utm_zone = args['UTM_zone']
    # ds-engine:
utm_zone = get_parameter('zone', 0)
print("***UTM zone***:", utm_zone)


# In[103]:

# Deducted paths
out_path           = os.path.join(dest_p, "patches", "png_16bits")
# Checks if exists already and creates it if not
pathlib.Path(out_path).mkdir(parents=True, exist_ok=True)
# Deducted constants
PATCH_SIZE_half = PATCH_SIZE//2
MARGIN_m        = int(PIXEL_RES_m * (PATCH_SIZE_half*SEC_COEF))


# ## A. Retrieves input data

# ###  I. Quick access to extraction reference: Dictionary

# In[93]:


# read the extraction ref .csv file
ref_df = pd.read_csv(extract_ref, sep=' ')
# Convert columns types to best suited types
ref_df = ref_df.convert_dtypes()
## CONVERSION to dictionary
Dref   = ref_df.set_index('gbifid').to_dict('index')
# Dref
print("Occurrences reference loaded")




# ### II. Retrieves {UTM_zone:{'tile': {'geom':MultiPolygon, 'points':{gbifid:Point}} }} Dictionary
#
# This dictionary allows to loop patch extraction by UTM zone & tile.

# In[94]:

data          = np.load(dico_utm_p, allow_pickle=True)
D_utm         = data['D'].item()


# ## B. Loop on tiles and occurrences

# In[96]:

# Initialisations
Lmissing_tar = []
Lempty_tar   = []
L_untarring  = []
L_TSocc      = []
L_month      = []

# Time explicit
Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, Lss = [], [], [], [], [], []
L_TSdezip = []


# log.info("Enters loop on tiles")
start = time.time()
# for tile in D_ord.items():
for tile in list(D_utm[utm_zone].keys()): #[0:1]: # Use [a:b] slicing   # [1059:1060]
    print("\n\ncurrent tile:", tile)
    start_tile = time.time()
    # Retrieves matching .tar tile_tar
    tar_f = os.path.join(tiles_p, tile+'.tar')
        # Checks if it does exists
    if not(os.path.isfile(tar_f)):
        # .tar does not exists
        Lmissing_tar.append(tile+'.tar')
    
    else:
        # .tar does exists
        # print("\tmatching .tar does exist")
        # print("\tNb of occurrences in this tile:", len(D_utm[utm_zone][tile]["points"]))

        # Access tile .tar info:
        with tarfile.open(tar_f) as tar:
            # List of TarInfo objects
            members   = tar.getmembers()
            # print("\ttarfile opened, members:", members)

            # Creates a temporary directory using the context manager to untar ALL tar products
            # tmpdirname exists until next TILE is processed
            with tempfile.TemporaryDirectory(dir = dest_p) as tmpdirname:
                # print("\t\ttemporary dir created:", tmpdirname)
                # Extracts ALL tile's  potential zip products in tmpdirname
                untar_start = time.time()
                tar.extractall(path=tmpdirname, members=members)
                L_untarring.append(time.time() - untar_start)
                # print("\t\tElapsed time:", time.time() - untar_start, " for  ALL "+tile+".tar products extraction")
                
                # Retrieves WGS84 -> UTM suited projection
                try:
                    UTM, projection = retrieves_tile_projection(tmpdirname)
                except IndexError:
                    Lempty_tar.append(tile)
                    print(".tar of tile:", tile, "is empty")
                else:
                    # Initializes Dopen dict
                    Dopen = {}

                    # For each contained occ -> gbifid
                    for gbifid in list(D_utm[utm_zone][tile]["points"].keys()):         #[0:1]:   #Occs
                        print("\t***Occ GBIFID***:", gbifid)
                        occ_start = time.time()
                        #if gbifid time-series isn't already completed =  if not 'TS_completed'
                        if not(Dref[gbifid]['TS_completed']):
                            # Initializes TS_dezip
                            TS_dezip = 0
                            # Retrieves occurrence wgs coordinates
                            occ_wgs = retrieves_occ_wgs_coordinates(D_utm[utm_zone], tile, gbifid)
                            # print("\tocc_wgs:", occ_wgs)
                            # Retrieves/Creates dictionary of flags
                            Dflags  = retrieves_or_creates_occ_flag_dict(Dref, gbifid)
                            # print("\tInitial Dflags:", Dflags)
                            # Lm_needed: Needed months bc product field empty in reference. Format: ('MM', 'YYYY')
                            Lm_needed = retrieves_needed_months(Dref, gbifid)
                            # print("\tLm_needed:", Lm_needed)
                            # List of needed & available months for this occurrence
                            Lmn_avail = lists_needed_and_available_months(members, Lm_needed, Dflags, tile)
                            # print("\tLmn_avail:", Lmn_avail)
                            for m in Lmn_avail:
                                # print("\n\t\tcurrent month:", m)
                                month_start = time.time()
                                # List all zips in tmpdirname matching current month
                                zips_m = glob.glob(os.path.join(tmpdirname, '_'.join([ tile,m[1],m[0] ]), '*.zip'))
                                # print("\t\tZip files retrieval:", time.time() - month_start)
                                # print("\t\tzips extracted in the temp dir:", zips_m)
                                # List of products with data available, sorted by cloud cover %
                                ss_start  = time.time()
                                Lc_sorted = lists_and_sorts_suited_products3(occ_wgs, zips_m, MARGIN_m, tile, Dflags, m, projection)
                                ss_time = time.time() - ss_start
                                Lss.append(ss_time)
                                # print("\t\tP suited and sorted elapsed time:", ss_time)
                                # print("\t\tNb of suited products:", len(Lc_sorted))
                                # print("\t\tSorted list of suited products:", Lc_sorted)
                                if len(Lc_sorted)!=0:
                                    # Measures while loop
                                    while_start = time.time()
                                    # Initializes index on Lc_sorted
                                    i = 0
                                    while (not(is_patch_extracted(Dref, gbifid, m)) and i<len(Lc_sorted)):
                                        # Reads and saves patch if successfully, otherwise increases i
                                        # i = reads_and_saves_patch(occ_wgs, Lc_sorted[i], Dref, Dflags, gbifid, m, i, tile, out_path, PATCH_SIZE_half, N)
                                        i, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, Dopen = reads_and_saves_patch_time_explicit_16b(occ_wgs, Lc_sorted[i], Dref, Dflags, gbifid, m, i, tile,
                                                                  out_path, PATCH_SIZE_half, N, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, UTM, projection, Dopen, sav_format=sav_format, bands=bands)
                                        TS_dezip += Ldezip[-1]
                                    # print("\t\tWhile loop elapsed time:", time.time() - while_start)
                                    # If no product assigned:
                                    if not(is_patch_extracted(Dref, gbifid, m)):
                                        # flags "No successful extraction among tile suited products"
                                        flagging(Dflags, m, "No successful extraction among occ. suited products")
                                L_month.append(time.time() - month_start)
                                # print("\t\tMonth extraction time:", time.time() - month_start)
                            # TS extraction time
                            L_TSocc.append(time.time() - occ_start)
                            # print("\t\tOccurrence TS extraction time:", time.time() - occ_start)
                            # TS total dezipping time
                            L_TSdezip.append(TS_dezip)
                            # print("\t\tOcc total TS dezipping time:", TS_dezip)
                            # Assigns Dflags in Dref for occurence gbifid
                            # print("\n\n\tDflags before assignation in Dref:", Dflags)
                            assigns_flags(Dref, Dflags, gbifid)
                            # Assigns 'TS_completed' if all months have been extracted
                            assigns_TS_completed(Dref, gbifid)
                            # print("\n\t\tDref[gbifid]['TS_completed']?:", Dref[gbifid]['TS_completed'])
                    # Closes every month product opened 10m subdatasets
                    [Dopen[prod].close() for prod in Dopen.keys()]
                    del Dopen
                    gc.collect()


# SAVE Dref with a given & distinct NAME per run & UTM zone
# Dref name
run = '_'.join(["Dref_UTM", str(utm_zone), datetime.today().strftime('%Y-%m-%d-%Hh%M')])
# Path to Dref folder
Drefs_out_p = os.path.join(dest_p,'Drefs')
if not(os.path.exists(Drefs_out_p)):
    os.mkdir(Drefs_out_p)
    print("Folder created: ", Drefs_out_p)
# Effective save
np.savez(os.path.join(Drefs_out_p, run),
         Dref = Dref)


# In[97]:


print("\n\nMissing tar files:", Lmissing_tar)
print("\n\nEmpty tar files:", Lempty_tar)
print(" Total elapsed time:", time.time() - start)
print("\nL_untarring:", np.mean(L_untarring))
print("\nL_TSocc:", np.mean(L_TSocc))
print("\nL_TSdezip:", np.mean(L_TSdezip))
print("\nL_month:", np.mean(L_month))
print("\n")
# Ldezip, Lpix_acess, LTpix_range, Lread, Lsave
print("\nLdezip:", np.mean(Ldezip))

print("\nLss:", np.mean(Lss))

print("\nLpix_acess:", np.mean(Lpix_acess))
print("\nLTpix_range:", np.mean(LTpix_range))
print("\nLread:", np.mean(Lread))
print("\nLsave:", np.mean(Lsave))
