import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import time
from datetime import date
import pyproj
import osgeo.gdal
import shapely.wkt
from shapely.ops import transform
from shapely.geometry import Point, MultiPoint, Polygon
import pandas as pd
import rasterio
from rasterio.windows import Window
from rasterio import plot
from PIL import Image

#
from coordinates_functions import *

## FLAGS FUNCTIONS

def retrieves_or_creates_occ_flag_dict(Dref, gbifid):
    """
    Test for each month if a flag list has already assigned and returns dict {"MM_YYYY":flag_list}
    params:
        - Dref : dict, occurrences reference
        - gbifid: unique ID of current occurrence
    """
    # Retrieves every month flag column name
    flags = [col for col in Dref[gbifid].keys() if "flag" in col]
    # Empty flag dict
    Dflags_occ  = {}
    # Loop on months
    for f in flags:
        # If a flag list already exists for this occurrence
        if isinstance(Dref[gbifid][f], list):
            # Retrieves flag list
            Dflags_occ[f] = Dref[gbifid][f]
        else:
            # Creates initial empty list
            Dflags_occ[f] = []
    return Dflags_occ


def flagging(Dflags_occ, m, msg):
    """
    Flags in Dflags_occ dict at month m with message msg.
    params:
        - Dflags_occ: dict {"MM_YYYY":flag_list} 
        - m     : month tuple with ('MM', 'YYYY') format
        - msg   : str, message to flag
    """
    # Retrieves month flag column
    month_str = '_'.join(["flag",m[0],m[1]])
    # Flags
    Dflags_occ[month_str].append(msg)
    #return Dflags


def assigns_flags(Dref, Dflags_occ, gbifid):
    """
    Assigns Dflags_occ in Dref for occ gbifid
    """
    # Retrieves every month flag column name
    flags = [col for col in Dref[gbifid].keys() if "flag" in col]
    # Loop on months
    for f in flags:
        Dref[gbifid][f] = Dflags_occ[f]


# TESTS

# occ_id = 1847942808
# print("Dref[occ_id] before:")
# Dref[occ_id]

# # Retrieves/ Creates the dictionary of flags for occurrence gbifid
# Dflags_occ = retrieves_or_creates_occ_flag_dict(Dref, occ_id)
# print("\tDflags_occ before flagging:")
# Dflags_occ

# # # TEST
# flagging(Dflags_occ, ('10','2020'), "oooh")
# flagging(Dflags_occ, ('11','2020'), "Ah!")
# flagging(Dflags_occ, ('02','2021'), "Impressive")
# print("\tDflags_occ after flagging :")
# Dflags_occ

# assigns_flags(Dref, Dflags_occ, occ_id)
# print("Dref[occ_id] after :")
# Dref[occ_id]

# # Witness
# Dref[2272777073]
