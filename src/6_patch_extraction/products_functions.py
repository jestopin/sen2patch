import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import time
from datetime import date
import pyproj
import osgeo.gdal
import shapely.wkt
from shapely.ops import transform
from shapely.geometry import Point, MultiPoint, Polygon
import pandas as pd
import rasterio
from rasterio.windows import Window
from rasterio import plot
from PIL import Image

#
from coordinates_functions import *
from flags_functions import *


## PRODUCTS FUNCTIONS


def retrieves_needed_months(Dref, gbifid):
    """
    Retrieves list of months with no assigned product = months with patch to extract from Dref gbifid occurrence.
    """
    # Retrieves every month prod column name
    prods = [col for col in Dref[gbifid].keys() if "prod" in col]
    # [Needed months]
    Lm_needed = [ (prod[5:7],prod[-4:]) for prod in prods if pd.isna(Dref[gbifid][prod]) ]
    return Lm_needed

# # Test
# print("Needed months:", retrieves_needed_months(Dref, 1847942808), '\n')
# # Verification
# Dref[1847942808]



def lists_needed_and_available_months(members, Lm_needed, Dflags, tile):
    """
    Compares needed months from Lm_needed list to available products in members.
    Results in returned list Lmn_avail. Flags needed but not available months.
    params:
        - members  : List of TarInfo objects from tile .tar
        - Lm_needed: List of needed months ("MM","YYYY")
        - Dflags   : Dictionary of current occurrence flags
        - tile     : str of current tile
    """
    # Star_avail: Set of available months in tile tar in ('MM', 'YYYY') format
    Star_avail = set([ (tinfo.name.split('_')[2][:2], tinfo.name.split('_')[1]) for tinfo in members ])
    # print("Star_avail:", Star_avail)

    # Lmn_avail = Among Lm_needed months, months available in tile .tar
    Lmn_avail   = [month for month in Lm_needed if month in Star_avail]

    # Months needed but not available in this tile products
    Lmn_not_avail = [month for month in Lm_needed if month not in Lmn_avail]
    # --> flags these months with:   "No available products in tileid"
    [ flagging(Dflags, m, "No avail. prod. in "+tile) for m in Lmn_not_avail ]
    
    return Lmn_avail

# # TEST on JZ: OK
# lists_needed_and_available_months(members, Lm_needed, Dflags, tile)




def is_patch_in_product(occ_wgs, prod_zip, MARGIN_m):
    """
    Defines if a given occurrence is in a targeted Sentinel-2 product data footprint, with a set margin.
    Returns a boolean.
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str of the path to the tested S2 product
        - MARGIN_m: int in meters defining the margin to respect when testing if the patch belongs to the product
    """
    # Retrieves prod footprint_wgs & UTM projection
    info_prod     = osgeo.gdal.Info(prod_zip).split('\n')
    footprint_wgs = shapely.wkt.loads([l for l in info_prod[:15] if 'FOOTPRINT' in l][0].split('=')[1])
    epsg_str      = [l for l in info_prod[-20:] if 'SUBDATASET_1_NAME' in l][0].split('EPSG_')[-1]
    UTM           = pyproj.CRS('EPSG:'+epsg_str)
    
    # WGS84 coordinates projection in UTM
    project       = pyproj.Transformer.from_crs(pyproj.CRS('EPSG:4326'), UTM, always_xy=True).transform
    footprint_utm = transform(project, footprint_wgs)
    Pocc_utm      = transform(project, Point(occ_wgs))
    
    # Buffer creation
    buffer_utm    = Pocc_utm.buffer(MARGIN_m, cap_style=3)
    Pcorners_utm  = MultiPoint(list(set(buffer_utm.exterior.coords)))
    # Boolean test
    return Pcorners_utm.within(footprint_utm)

# TEST OK

def retrieves_tile_projection(tmpdirname):
    """
    Used in Patch_Extraction3 to put in common projection retrieval for all occurrences (and months!) in a tile.
    """
    # All zip products
    prod_zips      = glob.glob(os.path.join(tmpdirname, '**/*.zip'), recursive=True)

    # Counter
    i = 0
    # Tries with first product
    info_prod     = osgeo.gdal.Info(prod_zips[i]).split('\n')
    # Tries to avoid corrupted metadata files
    while (info_prod is None and i<len(prod_zips)):
        i +=1
        info_prod = osgeo.gdal.Info(prod_zips[i]).split('\n')
    # print("gdal.Info():", len(info_prod), '\n', info_prod)
    
    # UTM retrieval
    epsg_str      = [l for l in info_prod if 'SUBDATASET_1_NAME' in l][0].split('EPSG_')[-1]
    UTM           = pyproj.CRS('EPSG:' + epsg_str)

    # WGS84 coordinates projection in UTM
    project = pyproj.Transformer.from_crs(pyproj.CRS('EPSG:4326'), UTM, always_xy=True).transform
    return UTM, project


def is_patch_in_product_time_explicit(occ_wgs, prod_zip, MARGIN_m):
    """
    Defines if a given occurrence is in a targeted Sentinel-2 product data footprint, with a set margin.
    Returns a boolean.
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str of the path to the tested S2 product
        - MARGIN_m: int in meters defining the margin to respect when testing if the patch belongs to the product
    """
    # Retrieves prod footprint_wgs & UTM projection
    info_start = time.time()
    info_prod = osgeo.gdal.Info(prod_zip).split('\n')
    footprint_wgs = shapely.wkt.loads([l for l in info_prod[:15] if 'FOOTPRINT' in l][0].split('=')[1])
    epsg_str = [l for l in info_prod[-20:] if 'SUBDATASET_1_NAME' in l][0].split('EPSG_')[-1]
    UTM = pyproj.CRS('EPSG:' + epsg_str)
    # print("\t\t\t...Retrieving product info (gdal.Info(), footprint,epsg):", time.time() - info_start)

    # WGS84 coordinates projection in UTM
    proj_start = time.time()
    project = pyproj.Transformer.from_crs(pyproj.CRS('EPSG:4326'), UTM, always_xy=True).transform
    footprint_utm = transform(project, footprint_wgs)
    Pocc_utm = transform(project, Point(occ_wgs))
    # print("\t\t\t...Projecting footprint and occ in UTM:", time.time() - proj_start)

    # Buffer creation
    buff_start = time.time()
    buffer_utm = Pocc_utm.buffer(MARGIN_m, cap_style=3)
    Pcorners_utm = MultiPoint(list(set(buffer_utm.exterior.coords)))
    # print("\t\t\t...Creating buffer & its corners around occurrence:", time.time() - buff_start)
    # Boolean test
    test_start = time.time()
    Test = Pcorners_utm.within(footprint_utm)
    # print("\t\t\t...Testing if buffer in UTM footprint:", time.time() - test_start)
    return Test



def is_patch_in_product_time_explicit3(occ_wgs, prod_zip, MARGIN_m, projection):
    """
    Defines if a given occurrence is in a targeted Sentinel-2 product data footprint, with a set margin.
    Returns a boolean.
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str of the path to the tested S2 product
        - MARGIN_m: int in meters defining the margin to respect when testing if the patch belongs to the product
    """
    # Retrieves prod footprint_wgs & UTM projection
    info_start    = time.time()
    info_prod     = osgeo.gdal.Info(prod_zip).split('\n')
    if info_prod is None:
        # Tries to avoid error raised when accessing corrupted metadata info
        return False
    footprint_wgs = shapely.wkt.loads([l for l in info_prod[:15] if 'FOOTPRINT' in l][0].split('=')[1])
    # print("\t\t\t...Retrieving product info wh gdal.Info() & footprint:", time.time() - info_start)

    # WGS84 coordinates projection in UTM
    proj_start    = time.time()
    footprint_utm = transform(projection, footprint_wgs)
    Pocc_utm      = transform(projection, Point(occ_wgs))
    # print("\t\t\t...Projecting footprint and occ in UTM:", time.time() - proj_start)

    # Buffer creation
    buff_start = time.time()
    buffer_utm = Pocc_utm.buffer(MARGIN_m, cap_style=3)
    Pcorners_utm = MultiPoint(list(set(buffer_utm.exterior.coords)))
    # print("\t\t\t...Creating buffer & its corners around occurrence:", time.time() - buff_start)
    # Boolean test
    test_start = time.time()
    Test = Pcorners_utm.within(footprint_utm)
    # print("\t\t\t...Testing if buffer in UTM footprint:", time.time() - test_start)
    return Test



def is_patch_extracted(Dref, gbifid, m):
    """
    Test in Dref if the occurrences gbifid already has a product assigned for month m, i.e. an extracted patch
    params:
        - Dref : dict, occurrences reference
        - gbifid: int, occurrence ID
        - m     : month tuple with ('MM', 'YYYY') format
    """
    # Retrieves month product column
    month_str = '_'.join(["prod",m[0],m[1]])
    # Tests
    return not(pd.isna(Dref[gbifid][month_str]))
# TEST OK


def assigns_prod(Dref, gbifid, m, prod_path):
    """
    Assigns in Dref prod to gbifid month m 
    """
    # Retrieves month prod column
    month_str = '_'.join(["prod",m[0],m[1]])
    # Assigns prod
    Dref[gbifid][month_str] = prod_path.split('/')[-1]

# # TEST
# print("Before:", is_patch_extracted(Dref, 1847942808, ('04','2020')))

# assigns_prod(Dref, 1847942808, ('04','2020'), "this_is_a_product.zip")
# print("After :", is_patch_extracted(Dref, 1847942808, ('04','2020')))



def retrieves_ccp(prod_zip):
    "Returns the cloud cover % of a given zip product"
    info_prod = osgeo.gdal.Info(prod_zip).split('\n')
    ccp       = [float(l.split('=')[-1]) for l in info_prod[:10] if 'CLOUD_COVERAGE_ASSESSMENT' in l][0]
    return ccp


def sort_Lcandidates(L):
    "Retrieves for each zip product in L its cloud cover % and returns L sorted by cloud cover %"
    if len(L)==1:
        return L
    else:
        Lt = [(z,retrieves_ccp(z)) for z in L]
        return [ l[0] for l in sorted(Lt, key=lambda t: t[1]) ]



def lists_and_sorts_suited_products(occ_wgs, zips_m, MARGIN_m, tile, Dflags, m):
    """
    Returns the list of suited products for this occurrence patch, sorted by ascending cloud cover %.
    Flags if no any suited product for this patch
    params:
        - occ_wgs : tuple (lon., lat.) WGS84 coordinates
        - zips_m  : list of str paths of zip products potentially containing the occurrence for this month
        - MARGIN_m: int in meters defining the margin to respect when testing if the patch belongs to the product
        - tile    : str, current tile
        - m       : tuple ("MM,"YYYY") current months
        - Dflags  : Current occurrence flags dictionary
        
    """
    # Initializes the zip products list containing the patch occurrence
    Lcand = []
    
    # for each potential month product =  each zip
    for prod_zip in zips_m:
        # Tests if product contains data for this occurrence patch
        if is_patch_in_product_time_explicit(occ_wgs, prod_zip, MARGIN_m):
            # Appends in candidate products list Lcandidates
            Lcand.append(prod_zip)

    if len(Lcand)==0:
        # flag "No product with patch data avail. in tileid"
        flagging(Dflags, m, "No product with patch data avail. in "+tile)
        return Lcand
    else:
        # Returns Lcand sorted by ascending clouds cover
        return sort_Lcandidates(Lcand)

# # TEST on JZ: OK


def lists_and_sorts_suited_products3(occ_wgs, zips_m, MARGIN_m, tile, Dflags, m, projection):
    """
    Returns the list of suited products for this occurrence patch, sorted by ascending cloud cover %.
    Flags if no any suited product for this patch
    params:
        - occ_wgs : tuple (lon., lat.) WGS84 coordinates
        - zips_m  : list of str paths of zip products potentially containing the occurrence for this month
        - MARGIN_m: int in meters defining the margin to respect when testing if the patch belongs to the product
        - tile    : str, current tile
        - m       : tuple ("MM,"YYYY") current months
        - Dflags  : Current occurrence flags dictionary
        
    """
    # Initializes the zip products list containing the patch occurrence
    Lcand = []
    
    # for each potential month product =  each zip
    start = time.time()
    for prod_zip in zips_m:
        # Tests if product contains data for this occurrence patch
        if is_patch_in_product_time_explicit3(occ_wgs, prod_zip, MARGIN_m, projection):
            # Appends in candidate products list Lcandidates
            Lcand.append(prod_zip)
    # print("\t\tTest appartenance for all potential prods:", time.time() - start)
    if len(Lcand)==0:
        # flag "No product with patch data avail. in tileid"
        flagging(Dflags, m, "No product with patch data avail. in "+tile)
        return Lcand
    else:
        # Returns Lcand sorted by ascending clouds cover
        start = time.time()
        Lsorted = sort_Lcandidates(Lcand)
        # print("\t\tSort suited products by cc%:", time.time() - start)
        return Lsorted
    
    


def assigns_TS_completed(Dref, gbifid):
    """
    Assigns True boolean if data for every needed month for occurrence gbifid has been extracted
    """
    if len(retrieves_needed_months(Dref, gbifid))==0:
        Dref[gbifid]['TS_completed'] = True
        # print("\n\t\tTS completed for occ:", gbifid)
# TO TEST ON JZ

