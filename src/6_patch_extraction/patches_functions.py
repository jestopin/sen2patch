import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import time
from datetime import date
import osgeo.gdal
import shapely.wkt
from shapely.ops import transform
from shapely.geometry import Point, MultiPoint, Polygon
import pandas as pd
import rasterio
from rasterio.windows import Window
from rasterio import plot
from rasterio.plot import reshape_as_raster, reshape_as_image
from PIL import Image
import pathlib
import cv2

#
from coordinates_functions import *
from flags_functions import *
from products_functions import *


## PATCH FUNCTIONS


def is_patch_in_pixels_range(pix, PATCH_SIZE_half, N):
    """
    Tests if targeted pixels are in the range (0,N) and flags it if its not the case
    """
    T = (0 <= (pix[0] - PATCH_SIZE_half) and (pix[0] + PATCH_SIZE_half) < N) and \
        (0 <= (pix[1] - PATCH_SIZE_half) and (pix[1] + PATCH_SIZE_half) < N)
    # Returns test in every case
    return T


# TEST OK


def retrieves_or_creates_patch_path(out_path, tile, gbifid):
    """
    Retrieves or creates the path 'out_path/tile/gbifid/'
    """
    UTM_zone = tile[:2]
    path = os.path.join(out_path,
                        UTM_zone,
                        tile,
                        str(gbifid)
                        )
    # Creates path and parents folders if they do not already exist
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    return path


# TEST OK


def saves_patches(RGB_r, IR, Dflags, m, Dref, gbifid, prod_zip, tile, out_path, i):
    """
    Saves RGB_r and IR arrays in .jpeg images in  out_path directory, subdirectory /UTM_zone/tile/gbifid.
    patch name = RGB/IR0_YYYY_MM_gbifid_.jpeg
    """
    # Retrieves or create data UTM/tile/occ folder
    patch_path = retrieves_or_creates_patch_path(out_path, tile, gbifid)
    # patch names creation
    RGB_name = '_'.join(['RGB', m[1], m[0], str(gbifid), '.jpeg'])
    IR_name = '_'.join(['IR0', m[1], m[0], str(gbifid), '.jpeg'])
    try:
        Image.fromarray((RGB_r * 255).astype(np.uint8)).save(os.path.join(patch_path, RGB_name))
        Image.fromarray((IR * 255).astype(np.uint8)).save(os.path.join(patch_path, IR_name))
    except:
        flagging(Dflags, m, "Error when saving data at: " + patch_path)
        i += 1
    else:
        flagging(Dflags, m, "Data successfully saved")
        # assigns product
        assigns_prod(Dref, gbifid, m, prod_zip)
    return i


# TEST OK


def reads_and_saves_patch(occ_wgs, prod_zip, Dref, Dflags, gbifid, m, i, tile, out_path, PATCH_SIZE_half, N):
    """
    Reads and saves patch data around occ_wgs occurrence.
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str to the path to the S2 product to be extracted
    """
    try:
        # Retrieves product info for the UTM projection
        UTM, project = retrieves_product_UTM_projection(prod_zip)
    except:
        flagging(Dflags, m, "retrieves_product_UTM_projection(" + prod_zip + ") raises an error")
        i += 1
    else:
        # WGS84 coordinates projection in UTM
        occ_utm = utm_from_wgs_coordinates(occ_wgs, project)
        try:
            # Gets 10m subdataset info
            sdata10_f = [ss_t[0] for ss_t in osgeo.gdal.Open(prod_zip).GetSubDatasets() if '10m' in ss_t[0]][0]
            # Opens 10m subdataset
            sdata10_o = rasterio.open(sdata10_f, driver='SENTINEL2', crs=UTM)
        except:
            flagging(Dflags, m, "Error raised by gdal.Open() or rasterio.open(), product: " + prod_zip)
            i += 1
        else:
            flagging(Dflags, m, "Successful data opening, product: " + prod_zip)
            try:
                # Nearest pixel to occ_utm
                pix = sdata10_o.index(occ_utm[0], occ_utm[1])
            except:
                flagging(Dflags, m, "Error when trying to access occ central pixel wh index() method: " + prod_zip)
                i += 1
            else:
                flagging(Dflags, m, "Successful access to occ central pixel wh index() method: " + prod_zip)
                # Verification on patch pixels range + flagging if necessary
                if not (is_patch_in_pixels_range(pix, PATCH_SIZE_half, N)):
                    # Patch pixels range is NOT respected
                    flagging(Dflags, m, "Pixels patch outside (0,N) range in product " + prod_zip)
                    i += 1
                else:
                    # Patch pixels range is respected
                    # Use of Window.from_slices((row_start, row_stop), (col_start, col_stop))
                    wind = Window.from_slices((pix[0] - PATCH_SIZE_half, pix[0] + PATCH_SIZE_half),
                                              (pix[1] - PATCH_SIZE_half, pix[1] + PATCH_SIZE_half))
                    try:
                        # Reads RGB & IR
                        RGB = sdata10_o.read([1, 2, 3], window=wind, out_dtype="float64")
                        IR = sdata10_o.read(4, window=wind, out_dtype="float64")
                    except:
                        flagging(Dflags, m, "Error when trying to read RGB/IR data: " + prod_zip)
                        i += 1
                    else:
                        flagging(Dflags, m, "Data successfully read: " + prod_zip)
                        # Normalizes and reorders bands from (3,n,m) to (n,m,3)
                        RGB_r = np.moveaxis(RGB / np.max(RGB), 0, 2)
                        # Normalization
                        IR /= np.max(IR)
                        # Saves RGB_r and IR patches
                        i = saves_patches(RGB_r, IR, Dflags, m, Dref, gbifid, prod_zip, tile, out_path, i)
            # Closes opened 10m dataset
            sdata10_o.close()
    return i


# TESTS on JZ: OK


def reads_and_saves_patch_time_explicit(occ_wgs, prod_zip, Dref, Dflags, gbifid, m, i, tile, out_path, PATCH_SIZE_half,
                                        N, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave):
    """
    Reads and saves patch data around occ_wgs occurrence.
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str to the path to the S2 product to be extracted
    """
    try:
        # Retrieves product info for the UTM projection
        UTM, project = retrieves_product_UTM_projection(prod_zip)
    except:
        flagging(Dflags, m, "retrieves_product_UTM_projection(" + prod_zip + ") raises an error")
        i += 1
    else:
        # WGS84 coordinates projection in UTM
        occ_utm = utm_from_wgs_coordinates(occ_wgs, project)
        try:
            dezip_start = time.time()
            # Gets 10m subdataset info
            sdata10_f = [ss_t[0] for ss_t in osgeo.gdal.Open(prod_zip).GetSubDatasets() if '10m' in ss_t[0]][0]
            # Opens 10m subdataset
            sdata10_o = rasterio.open(sdata10_f, driver='SENTINEL2', crs=UTM)
            dezip_time = time.time() - dezip_start
            Ldezip.append(dezip_time)
            # print("Dezip elapsed time:", dezip_time)
        except:
            flagging(Dflags, m, "Error raised by gdal.Open() or rasterio.open(), product: " + prod_zip)
            i += 1
        else:
            flagging(Dflags, m, "Successful data opening, product: " + prod_zip)
            try:
                pix_access_start = time.time()
                # Nearest pixel to occ_utm
                pix = sdata10_o.index(occ_utm[0], occ_utm[1])
                pix_access_time = time.time() - pix_access_start
                Lpix_acess.append(pix_access_time)
                # print("Pix access elapsed time:", pix_access_time)
            except:
                flagging(Dflags, m, "Error when trying to access occ central pixel wh index() method: " + prod_zip)
                i += 1
            else:
                flagging(Dflags, m, "Successful access to occ central pixel wh index() method: " + prod_zip)
                # Verification on patch pixels range + flagging if necessary

                Tpix_range_start = time.time()
                if not (is_patch_in_pixels_range(pix, PATCH_SIZE_half, N)):
                    Tpix_range_time = time.time() - Tpix_range_start
                    LTpix_range.append(Tpix_range_time)
                    # print("Pix range elapsed time:", Tpix_range_time)
                    # Patch pixels range is NOT respected
                    flagging(Dflags, m, "Pixels patch outside (0,N) range in product " + prod_zip)
                    i += 1
                else:
                    Tpix_range_time = time.time() - Tpix_range_start
                    LTpix_range.append(Tpix_range_time)
                    # print("Pix range elapsed time:", Tpix_range_time)
                    # Patch pixels range is respected
                    # Use of Window.from_slices((row_start, row_stop), (col_start, col_stop))
                    wind = Window.from_slices((pix[0] - PATCH_SIZE_half, pix[0] + PATCH_SIZE_half),
                                              (pix[1] - PATCH_SIZE_half, pix[1] + PATCH_SIZE_half))
                    try:
                        read_start = time.time()
                        # Reads RGB & IR
                        RGB = sdata10_o.read([1, 2, 3], window=wind, out_dtype="float64")
                        IR = sdata10_o.read(4, window=wind, out_dtype="float64")
                        read_time = time.time() - read_start
                        Lread.append(read_time)
                        # print("Read elapsed time:", read_time)
                    except:
                        flagging(Dflags, m, "Error when trying to read RGB/IR data: " + prod_zip)
                        i += 1
                    else:
                        flagging(Dflags, m, "Data successfully read: " + prod_zip)
                        # Normalizes and reorders bands from (3,n,m) to (n,m,3)
                        RGB_r = np.moveaxis(RGB / np.max(RGB), 0, 2)
                        # Normalization
                        IR /= np.max(IR)

                        save_start = time.time()
                        # Saves RGB_r and IR patches
                        i = saves_patches(RGB_r, IR, Dflags, m, Dref, gbifid, prod_zip, tile, out_path, i)
                        save_time = time.time() - save_start
                        Lsave.append(save_time)
                        # print("Save elapsed time:", save_time)
            # Closes opened 10m dataset
            sdata10_o.close()
    return i, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave


def reads_and_saves_patch_time_explicit4(occ_wgs, prod_zip, Dref, Dflags, gbifid, m, i, tile, out_path, PATCH_SIZE_half,
                                         N, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, UTM, projection, Dopen):
    """
    Reads and saves patch data around occ_wgs occurrence. Dopens stores the tile 10m opened subdataset for further use with other occurrences
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str to the path to the S2 product to be extracted
    """
    # WGS84 coordinates projection in UTM
    occ_utm = utm_from_wgs_coordinates(occ_wgs, projection)
    try:
        dezip_start = time.time()
        prod_str = prod_zip.split('/')[-1]
        if prod_str not in Dopen:
            # Gets 10m subdataset info
            sdata10_f = [ss_t[0] for ss_t in osgeo.gdal.Open(prod_zip).GetSubDatasets() if '10m' in ss_t[0]][0]
            # Opens 10m subdataset
            Dopen[prod_str] = rasterio.open(sdata10_f, driver='SENTINEL2', crs=UTM)

        dezip_time = time.time() - dezip_start
        Ldezip.append(dezip_time)
        # print("Dezip elapsed time:", dezip_time)
    except:
        flagging(Dflags, m, "Error raised by gdal.Open() or rasterio.open(), product: " + prod_zip)
        i += 1
    else:
        flagging(Dflags, m, "Successful data opening, product: " + prod_zip)
        try:
            pix_access_start = time.time()
            # Nearest pixel to occ_utm
            pix = Dopen[prod_str].index(occ_utm[0], occ_utm[1])
            pix_access_time = time.time() - pix_access_start
            Lpix_acess.append(pix_access_time)
            # print("Pix access elapsed time:", pix_access_time)
        except:
            flagging(Dflags, m, "Error when trying to access occ central pixel wh index() method: " + prod_zip)
            i += 1
        else:
            flagging(Dflags, m, "Successful access to occ central pixel wh index() method: " + prod_zip)
            # Verification on patch pixels range + flagging if necessary

            Tpix_range_start = time.time()
            if not (is_patch_in_pixels_range(pix, PATCH_SIZE_half, N)):
                Tpix_range_time = time.time() - Tpix_range_start
                LTpix_range.append(Tpix_range_time)
                # print("Pix range elapsed time:", Tpix_range_time)
                # Patch pixels range is NOT respected
                flagging(Dflags, m, "Pixels patch outside (0,N) range in product " + prod_zip)
                i += 1
            else:
                Tpix_range_time = time.time() - Tpix_range_start
                LTpix_range.append(Tpix_range_time)
                # print("Pix range elapsed time:", Tpix_range_time)
                # Patch pixels range is respected
                # Use of Window.from_slices((row_start, row_stop), (col_start, col_stop))
                wind = Window.from_slices((pix[0] - PATCH_SIZE_half, pix[0] + PATCH_SIZE_half),
                                          (pix[1] - PATCH_SIZE_half, pix[1] + PATCH_SIZE_half))
                try:
                    read_start = time.time()
                    # Reads RGB & IR
                    RGB = Dopen[prod_str].read([1, 2, 3], window=wind, out_dtype="float64")
                    IR = Dopen[prod_str].read(4, window=wind, out_dtype="float64")
                    read_time = time.time() - read_start
                    Lread.append(read_time)
                    # print("Read elapsed time:", read_time)
                except:
                    flagging(Dflags, m, "Error when trying to read RGB/IR data: " + prod_zip)
                    i += 1
                else:
                    flagging(Dflags, m, "Data successfully read: " + prod_zip)
                    # Normalizes and reorders bands from (3,n,m) to (n,m,3)
                    RGB_r = np.moveaxis(RGB / np.max(RGB), 0, 2)
                    # Normalization
                    IR /= np.max(IR)

                    save_start = time.time()
                    # Saves RGB_r and IR patches
                    i = saves_patches(RGB_r, IR, Dflags, m, Dref, gbifid, prod_zip, tile, out_path, i)
                    save_time = time.time() - save_start
                    Lsave.append(save_time)
                    # print("Save elapsed time:", save_time)
        # # Closes opened 10m dataset
        # sdata10_o.close()
    return i, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, Dopen



def reads_and_saves_patch_time_explicit_16b(occ_wgs, prod_zip, Dref, Dflags, gbifid, m, i, tile, out_path, PATCH_SIZE_half,
                                         N, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, UTM, projection, Dopen, sav_format='.png', bands='10m'):
    """
    Reads and saves patch data around occ_wgs occurrence. Dopens stores the tile 10m opened subdataset for further use with other occurrences
    params:
        - occ_wgs : (lon,lat) tuple of decimal WGS84 coordinates
        - prod_zip: str to the path to the S2 product to be extracted
    """
    # WGS84 coordinates projection in UTM
    start = time.time()
    occ_utm = utm_from_wgs_coordinates(occ_wgs, projection)
    # print("\t\t***utm_from_wgs_coordinates time ex for each occurrence:***", time.time() - start)
    try:
        dezip_start = time.time()
        prod_str = prod_zip.split('/')[-1]
        if prod_str not in Dopen:
            # Gets 10m subdataset info
            sdata10_f = [ss_t[0] for ss_t in osgeo.gdal.Open(prod_zip).GetSubDatasets() if bands in ss_t[0]][0]
            # Opens 10m subdataset
            Dopen[prod_str] = rasterio.open(sdata10_f, driver='SENTINEL2', crs=UTM)

        dezip_time = time.time() - dezip_start
        Ldezip.append(dezip_time)
        # print("Dezip elapsed time:", dezip_time)
    except:
        flagging(Dflags, m, "Error raised by gdal.Open() or rasterio.open(), product: " + prod_zip.split('/')[-1])
        i += 1
    else:
        flagging(Dflags, m, "Successful data opening, product: " + prod_zip.split('/')[-1])
        try:
            pix_access_start = time.time()
            # Nearest pixel to occ_utm
            pix = Dopen[prod_str].index(occ_utm[0], occ_utm[1])
            pix_access_time = time.time() - pix_access_start
            Lpix_acess.append(pix_access_time)
            # print("Pix access elapsed time:", pix_access_time)
        except:
            flagging(Dflags, m, "Error when trying to access occ central pixel wh index() method: " + prod_zip.split('/')[-1])
            i += 1
        else:
            flagging(Dflags, m, "Successful access to occ central pixel")
            # Verification on patch pixels range + flagging if necessary

            Tpix_range_start = time.time()
            if not (is_patch_in_pixels_range(pix, PATCH_SIZE_half, N)):
                Tpix_range_time = time.time() - Tpix_range_start
                LTpix_range.append(Tpix_range_time)
                # print("Pix range elapsed time:", Tpix_range_time)
                # Patch pixels range is NOT respected
                flagging(Dflags, m, "Pixels patch outside (0,N) range in product " + prod_zip.split('/')[-1])
                i += 1
            else:
                Tpix_range_time = time.time() - Tpix_range_start
                LTpix_range.append(Tpix_range_time)
                # print("Pix range elapsed time:", Tpix_range_time)
                # Patch pixels range is respected
                # Use of Window.from_slices((row_start, row_stop), (col_start, col_stop))
                wind = Window.from_slices((pix[0] - PATCH_SIZE_half, pix[0] + PATCH_SIZE_half),
                                          (pix[1] - PATCH_SIZE_half, pix[1] + PATCH_SIZE_half))
                try:
                    read_start = time.time()
                    # Reads RGB & IR
                    RGB = Dopen[prod_str].read([1, 2, 3], window=wind, out_dtype=np.uint16)
                    IR = Dopen[prod_str].read(4, window=wind, out_dtype=np.uint16)
                    read_time = time.time() - read_start
                    Lread.append(read_time)
                    # print("Read elapsed time:", read_time)
                except:
                    flagging(Dflags, m, "Error when trying to read RGB/IR data: " + prod_zip.split('/')[-1])
                    i += 1
                else:
                    flagging(Dflags, m, "Data successfully read")
                    # Normalizes and reorders bands from (3,n,m) to (n,m,3)
                    RGB_r = reshape_as_image(RGB)

                    save_start = time.time()
                    # Saves RGB_r and IR patches
                    i = saves_patches_16b(RGB_r, IR, Dflags, m, Dref, gbifid, prod_zip, tile, out_path, i, sav_format=sav_format)
                    save_time = time.time() - save_start
                    Lsave.append(save_time)
                    # print("Save elapsed time:", save_time)
        # # Closes opened 10m dataset
        # sdata10_o.close()
    return i, Ldezip, Lpix_acess, LTpix_range, Lread, Lsave, Dopen


def saves_patches_16b(RGB_r, IR, Dflags, m, Dref, gbifid, prod_zip, tile, out_path, i, sav_format='.png'):
    """
    Saves RGB_r and IR arrays in 16b .png images in  out_path directory, subdirectory /UTM_zone/tile/gbifid.
    patch name = RGB/IR0_YYYY_MM_gbifid_.png
    """
    # Retrieves or create data UTM/tile/occ folder
    patch_path = retrieves_or_creates_patch_path(out_path, tile, gbifid)
    # patch names creation
    RGB_name = '_'.join(['RGB', m[1], m[0], str(gbifid), sav_format])
    IR_name = '_'.join(['IR0', m[1], m[0], str(gbifid), sav_format])
    try:
        cv2.imwrite(os.path.join(patch_path, RGB_name), RGB_r)
        cv2.imwrite(os.path.join(patch_path, IR_name) , IR)
    except:
        flagging(Dflags, m, "Error when saving data at: " + patch_path)
        i += 1
    else:
        flagging(Dflags, m, "Data successfully saved")
        # assigns product
        assigns_prod(Dref, gbifid, m, prod_zip)
    return i