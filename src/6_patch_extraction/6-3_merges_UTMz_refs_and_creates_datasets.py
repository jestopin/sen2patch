#!/usr/bin/env python
# coding: utf-8

# This script merges the different references resulting from the parallel patch extraction of different UTM zones.
# It also spots the occurrences extracted various times (because near products limits)
# and keeps in final dataset the TS with the maximum number of extracted months (if various, keeps one arbitrarily).
# Unkept duplicates are listed in a distinct reference than the final dataset and
# matching patches will be moved to a different folder by 6-4_moves_duplicates_patches.py.



# ## I. Imports and input data

# In[1]:

import numpy as np
import os
import os.path
import glob
import pandas as pd


# In[2]:


# *** INPUT PATHS ***
data_p = "../data/"         # dest_p provided in 6-1
out_p  = "../data/occurrences/final_datasets/"
# Deducted paths
PATCHES_PATH = os.path.join(data_p, "patches", "png_16bits")  # JZ
DREFS_PATH   = os.path.join(data_p, "Drefs")                  # JZ


# ### 1) Utility functions

# In[3]:


def nb_extracted_months(row):
    if row['TS_completed']:
        return 12
    else:
        # Extracted months counter
        cmp = 0
        # Retrieves month prods list:
        prods = [idx for idx in row.index if 'prod' in idx]
        for p in prods:
            if not(pd.isna(row[p])):
                cmp +=1
        return cmp


# In[4]:


# # TEST OK
# prods = [idx for idx in D[z].iloc[2].index if 'prod' in idx]
# # prods
# not(pd.isna(D[z].iloc[2]['prod_03_2020']))
# 
# A = D[z].iloc[0]
# A['TS_completed'] = False
# A['prod_03_2020'] = 'sdfd'
# A['prod_02_2021'] = 'sdfd'
# nb_extracted_months(A)


# ### 2) Loading loop

# In[5]:


# Choice
selected_zones  = np.arange(1,61)       # ALL zones, JZ

# Dict of Drefs
D = {}


# In[6]:


# Loads the Dref matching UTM zone z
for z in selected_zones:
    Lz_dref_p = glob.glob(os.path.join(DREFS_PATH, "Dref_UTM_"+str(z)+"_*"))
    print("Nb of dict for UTM zone number "+str(z)+":", len(Lz_dref_p))

    # if data has been extracted and thus referenced for this UTM zone
    if len(Lz_dref_p)>=1:
        z_dref_p  = Lz_dref_p[0]
        print("\tChosen dict\t:", z_dref_p)

        # # Effective loading
        data = np.load(z_dref_p, allow_pickle=True)
        D[z] = pd.DataFrame.from_dict(data['Dref'].item(), orient='index').reset_index()
        # rename index
        D[z].rename(columns={'index': 'id'}, inplace=True)
        print("Removing empty rows...")
        print("\t...Before\t:", D[z].shape)

        # Adds nb_extracted_months column
        D[z]['nb_extracted_months'] = D[z].apply (lambda row: nb_extracted_months(row), axis=1)
        # Deletes empty rows
        D[z] = D[z][D[z].nb_extracted_months != 0]
        print("\t...After\t:", D[z].shape)


# ## III. Final datasets

# - Merge all Drefs

# In[7]:


# All the gbifid with data extracted concatenated in df
df  = pd.concat([D[z] for z in selected_zones], ignore_index=True)
print("Merged dataset\t:", df.shape)
# df


# - Unique/Duplicates?

# In[8]:


# Number of TS duplicates per gbifid
print(df.groupby(['id'], sort=False).size(), '\n')


# Total nb of rows
print("Total nb of rows\t:", df.shape[0])
# All unique gbifid rows
df_u = df.groupby(['id'], sort=False).filter(lambda x: len(x)==1)
print("Nb of unique gbifids\t:", df_u.shape[0], ',', int(df_u.shape[0]/df.shape[0]*100), '%')
# df_u

# All duplicates rows
df_dpls = df.groupby(['id'], sort=False).filter(lambda x: len(x) > 1)
print("Nb of duplicate rows\t:", df_dpls.shape[0], ',', int(df_dpls.shape[0]/df.shape[0]*100), '%')
# df_dpls


# - Keep highest nb_of_extracted_month row for final **dataset**:

# In[9]:


# dataset = Keep the first gbifid row after having sorted by number of extracted months 
grouped = df.groupby(['id'], sort=False, group_keys=False)
dataset = grouped.apply(lambda x: x.sort_values(('nb_extracted_months'), ascending=False).head(1))
print("FINAL dataset\t\t:", dataset.shape)
dataset


# - Dismissed rows in **duplicates**:

# In[10]:


# duplicates = df - dataset
duplicates = pd.concat([df, dataset]).loc[df.index.difference(dataset.index)]
print("DUPLICATES dataset\t:", duplicates.shape)
duplicates


# - Verification

# In[22]:


print("Dataset + Duplicates = All occurrences?\t:", (dataset.shape[0] + duplicates.shape[0] == df.shape[0]))


# - Recreates dicts for quicker access to dataset and duplicates

# In[12]:


## CONVERSION to dictionary
Ddataset    = dataset.set_index('id').to_dict('index')
Dduplicates = duplicates.set_index('id').to_dict('index')


# In[13]:


# #TEST
# # Dduplicates
# print(Dduplicates[1056712941])
# len(Dduplicates.keys()), Dduplicates.keys()
# Ddataset[1056712941]


# - Saves **dataset / duplicates** dictionaries

# In[14]:
# Checks if exists already and creates it if not
if not(os.path.exists(out_p)):
    os.mkdir(out_p)
    print("Folder created: ", out_p)


outfile_Ddataset    = os.path.join(out_p, 'Ddataset_default.npz')
outfile_Dduplicates = os.path.join(out_p, 'Dduplicates_default.npz')

np.savez(outfile_Ddataset, D = Ddataset)
np.savez(outfile_Dduplicates, D = Dduplicates)
print("\t...datasets saved at:", out_p)


# - Tries to load them

# In[15]:


# # dataset
# data1     = np.load(outfile_Ddataset, allow_pickle=True)
# Ddataset2 = data1['D'].item()

# # dataset
# data2        = np.load(outfile_Dduplicates, allow_pickle=True)
# Dduplicates2 = data2['D'].item()


# In[16]:


# TEST OK
# Ddataset2.keys()
# # Dduplicates2.keys()