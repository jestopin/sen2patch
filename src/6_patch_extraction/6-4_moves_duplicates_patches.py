#!/usr/bin/env python
# coding: utf-8


# Moves the patches pointed by the duplicates reference dataset_p created by 6-3 script into a different folder named *new_dir_p*.
# Last line to uncomment before using.

# ## I. Imports and input data

# In[1]:
import numpy as np
import os
import os.path
import pathlib
import shutil
import glob
import pandas as pd


# In[2]:


# *** INPUT PATHS ***
patch_p   = "../data/patches/png_16bits/"  # JZ
dataset_p = "../data/occurrences/final_datasets/Dduplicates_default.npz"
new_dir_p = "../data/patches/png_16bits/duplicates/"
# *******************



# Checks if exists already and creates it if not
if not(os.path.exists(new_dir_p)):
    os.mkdir(new_dir_p)
    print("Folder created: ", new_dir_p)


# - Saves **dataset / duplicates** dictionaries

# - Tries to load them

# In[15]:


# duplicates
data         = np.load(dataset_p, allow_pickle=True)
Dduplicates  = data['D'].item()
print("len(Dduplicates)\t:", len(Dduplicates))

# Converts duplicates dictionary to dataframe
duplicates = pd.DataFrame.from_dict(Dduplicates, orient='index').reset_index()
# rename index
duplicates.rename(columns={'index': 'id'}, inplace=True)
print("duplicates.shape\t:", duplicates.shape)


# ## IV. Moves duplicates files in another directory

# - Utility function

# In[17]:


def retrieves_row_files(row):
    # files = list of ABSOLUTE paths to the patches of the inputted occurrence row
    # Retrieves month prods list:
    prods = [idx for idx in row.index if 'prod' in idx]
    # List of files
    files = []
    # Loop on all months
    for p in prods: 
        if not(pd.isna(row[p])):
            # Retrieves product's tile
            tile = row[p].split('_')[5][1:]
            # Retrieves product's UTM zone
            zone = tile[:2]
            # ABSOLUTE paths
            path = os.path.join(patch_p, zone, tile, str(row['id']))
            # Retrieves month & year
            month= p.split('_')[1]
            year = p.split('_')[2]
            # Images' names
            name = '_'.join(['???', year, month, str(row['id']), '.png'])
            # Appends matching files
            files += glob.glob(os.path.join(path, name))
    return files


# In[18]:


# TEST
# A = duplicates.iloc[0]
# retrieves_row_files(A)


# - Function to move files

# In[19]:


def moves_df_files(df, dest_dir):
    """Moves undesired replicate patches from df to dest_dir."""
    # files_series = series of lists. Each list (one per occ) contains the paths to the month patches to move
    files_series = df.apply(lambda row: retrieves_row_files(row), axis=1)
    # Loop on them
    for i, Lrow in enumerate(files_series):
        print("\t...occ", (i+1), "/", len(Dduplicates), "\t:", i/len(Dduplicates)*100, "% done.")
        for file_fp in Lrow:
            # Retrieves file name only
            file_ly = file_fp.split('/')[-1]
            # Retrieves UTM_zone/tile/gbifid folders tree & reconstructs path at dest
            folders = '/'.join(file_fp.split('/')[-4:-1])
            dest_p  = os.path.join(dest_dir, folders)
            pathlib.Path(dest_p).mkdir(parents=True, exist_ok=True)
            # COPIES/MOVES files
            shutil.move(file_fp,
                        os.path.join(dest_p, file_ly)
                       )


# In[20]:

### Moves duplicates files in another place, TO UNCOMMENT
# moves_df_files(duplicates, new_dir_p)
###