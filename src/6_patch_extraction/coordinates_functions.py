import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import time
from datetime import date
import pyproj
import osgeo.gdal
import shapely.wkt
from shapely.ops import transform
from shapely.geometry import Point, MultiPoint, Polygon
import pandas as pd
import rasterio
from rasterio.windows import Window
from rasterio import plot
from PIL import Image


## COORDINATES FUNCTIONS

def retrieves_occ_wgs_coordinates(Dord, tile, gbifid):
    """
    Returns !(lon, lat)! WGS84 coordinates matching gbifid.
    params:
        - Dord  : dict, { 'tile': { 'geom': MultiPolygon, 'points': { gbifid:Point} } }
        - tile  : str of current tile
        - gbifid: unique ID of current occurrence
    """
    # Access dict
    P   = Dord[tile]['points'][gbifid]
    return (P.x,P.y)                       # !(lon, lat)!


# # TEST OK
# retrieves_occ_wgs_coordinates(Dord, '33VVE', 2272777073)


def retrieves_product_UTM_projection(prod_zip):
    # Retrieves product info for the UTM projection
    info_prod = osgeo.gdal.Info(prod_zip).split('\n')
    
    # Retrieves UTM code
    epsg_str  = [l for l in info_prod[-20:] if 'SUBDATASET_1_NAME' in l][0].split('EPSG_')[-1]
    UTM       = pyproj.CRS('EPSG:'+epsg_str)
    # Matching projection from WGS84 to UTM zone
    project   = pyproj.Transformer.from_crs(pyproj.CRS('EPSG:4326'), UTM, always_xy=True).transform
    
    return UTM, project

# TEST OK


def utm_from_wgs_coordinates(occ_wgs, project):
    """
    params:
        - occ_wgs : tuple, (lon,lat) WGS84 coordinates
        - project : pyproj.Transformer, WGS84 to UTM tile projection
    out:
        - matching tuple of UTM coordinaes
    """
    Pocc_utm     = transform(project, Point(occ_wgs))
    return (Pocc_utm.x, Pocc_utm.y)

# TEST OK
