#!/usr/bin/env python3
# coding: utf-8

# # S2 products download
# - .py file rather than .ipynb to easily launch on cluster
# - Downloads **L1C products** from the OrderedDict() products specifications outputted by *3_API_product_queries.ipynb*.
# - Allows the user to download targeted products subset per subset because huge data amounts can be implicated.
# - Sci-hub account needed


# In[9]:


import numpy as np
from collections import OrderedDict
from sentinelsat import SentinelAPI
import os
import pathlib
import time
import random
import argparse
np.random.seed(0)


# ### INLINE Argument
parser = argparse.ArgumentParser(description='Downloads a subset of S2 L1C products from inline input dictionary path in products_file_path.')

parser.add_argument("-ss", "--subset_start", help="Subset lower boundary: range(ss,se), must be an int", default=0, type=int)
parser.add_argument("-se", "--subset_end", help="Subset higher boundary: range(ss,se), must be an int", default=2, type=int)
parser.add_argument("-a", "--all", help="Download all products from targeted dict", action='store_true')

parser.add_argument("-u", "--username", help="Scihub Copernicus username")
parser.add_argument("-psd", "--password", help="PUT '' AROUND THE PASSWORD. Scihub Copernicus password (visible, use stupid password and not personal one...)")

parser.add_argument("-src_p", "--src_path", help="path to .npz containing targeted products files to download in a dict. No default.")
parser.add_argument("-dest_p", "--dest_path", default="../data/products/by_month/", help="Destination path for downloaded products.")


args                             = vars(parser.parse_args())
ss, se                           = args['subset_start'] , args['subset_end']
download_all                     = args['all']
username, password               = args['username'], "visible_password!!!" # args['password']
products_file_path, zip_L1C_path = args['src_path'], args['dest_path']
print("Range     :", ss,se)
print("Username  :", '*'+username+'*')
print("Password  :", '*'+password+'*')
print("Prod. file:", '*'+products_file_path+'*')
print()
subset       = range(ss,se)
# **************


# ### API connection
# In[11]:
api = SentinelAPI(username, password)


# ### Loads S2 products dictionary

# In[12]:


# Loads result
data     = np.load(products_file_path, allow_pickle = True)
products = data['products'].item()

print("Class :", type(products), "\nLength:", len(products))
Lunfound = list(data['Lunfound'])


# Retrieves product_id directory file name
fname	 = products_file_path.split('/')[-1]
# Retrieves month path
month_path = os.path.join(zip_L1C_path, fname[:-4]+'/')
pathlib.Path(month_path).mkdir(parents=True, exist_ok=True)

# ## Download all/a subset of targeted products

# In[13]:

if download_all:
    # *** ALL PRODUCTS
    print("Total products size to downlad: ", api.get_products_size(products), "GB\n")
    api.download_all(products, directory_path = month_path)

else:
    # *** SUBSET
    print("\tTotal products size: ", api.get_products_size(products), "GB\n")
    print("\tSubset             : ", subset)
    prods = OrderedDict([list(products.items())[i] for i in subset])

    # Download
    print("\tProducts:", prods)
    print("\tProducts to download: ", api.get_products_size(prods), "GB")
    api.download_all(prods, directory_path = month_path)


