import numpy as np
import matplotlib.pyplot as plt
import os
import glob
import cv2
from datetime import datetime

# *** HORIZONTAL PLOT *** # TS NORMALISED

def visualizes(occ_p, TS_norm=False, fig_p=None, orient='H'):
    """
    Plots the RGB/IR 16 bits PNG patches storred locally in occ_p.
    By default: normalization per patch, no figure saving and horizontal orientation.
    Argts:
        - occ_p  : str      ,             path to the directory containing the 16 bits PNG patches.
        - TS_norm: bool     , optional    put to True to draw the TS normalized globally.
        - fig_p  : str      , optional    if provided, saves the corresponding figure to this path.
        - orient : {'H','V'}, optional    Figure orientation 'H' for horizontal 'V' for vertical.
        
    """
    # Retrieves gbifid
    gbifid = int(occ_p.split('/')[-1])

    # Retrieves files
    files     = glob.glob(occ_p+'/*')
    RGB_files = sorted([f for f in files if 'RGB' in f]) #Sorted by date
    IR_files  = sorted([f for f in files if 'IR0' in f])
    
    if len(RGB_files)>=1 or len(IR_files)>=1:
        
        #Initializes data structures
        D_RGB = {}
        D_IR  = {}

        # LOADS everything
            # RGB
        for i in range(len(RGB_files)):
            # Month date
            date_str = RGB_files[i].split('/')[-1][4:11]
            month    = datetime.strftime(datetime.strptime(date_str, '%Y_%m'), '%b %y')
            # CV2 PNG 16 BITS
            D_RGB[month] = cv2.imread(RGB_files[i], cv2.IMREAD_UNCHANGED)

            # IR
        for i in range(len(IR_files)):
            # Month date
            date_str = IR_files[i].split('/')[-1][4:11]
            month    = datetime.strftime(datetime.strptime(date_str, '%Y_%m'), '%b %y')
            # CV2 PNG 16 BITS
            D_IR[month]  = cv2.imread(IR_files[i] , cv2.IMREAD_UNCHANGED)


        if TS_norm:
            # DETERMINES RGB/IR TS max
            if len(D_RGB)>=1:
                RGB_TS_max = np.max([np.max(D_RGB[k]) for k in D_RGB])
                print("TS maxs: \n\tRGB:", RGB_TS_max)
            if len(D_IR)>=1:
                IR_TS_max  = np.max([np.max(D_IR[k]) for k in D_IR])
                print('\tIR :', IR_TS_max)


        # Union of months
        S  = set().union(*[D_RGB, D_IR])
        # Sorted list of the union month
        Ls = sorted(S, key=lambda date_str: datetime.strptime(date_str, '%b %y'))

        if len(Ls)==0:
            print("No available data here for occurrence", gbifid)
        elif len(Ls)==1:
            # Only one element
            # Depending on orient value:
            if orient=='H': #default
                # PLOTS patches
                fig, axs = plt.subplots(2, len(Ls), figsize=(6*len(Ls),11) )

                # Loop on union keys
                for i,k in enumerate(Ls):
                    # Name & disable axis
                    axs[0].set_title(k, fontsize=50)
                    axs[0].axis(False)
                    axs[1].axis(False)

                    # RGB patch
                    if k in D_RGB.keys():
                        if TS_norm:
                            axs[0].imshow(D_RGB[k]/RGB_TS_max)
                        else:
                            axs[0].imshow(D_RGB[k]/np.max(D_RGB[k]))
                    # IR patch
                    if k in D_IR.keys():
                        if TS_norm:
                            axs[1].imshow(D_IR[k]/IR_TS_max)
                        else:
                            axs[1].imshow(D_IR[k]/np.max(D_IR[k]))
                plt.show()

            elif orient=='V':
                # PLOTS patches
                fig, axs = plt.subplots(len(Ls), 2, figsize=(11,6*len(Ls)) )

                # Loop on union keys
                for i,k in enumerate(Ls):
                    # Name & disable axis
                    axs[0].set_title(k, fontsize=35)
                    axs[0].axis(False)
                    axs[1].axis(False)

                    # RGB patch
                    if k in D_RGB.keys():
                        if TS_norm:
                            axs[0].imshow(D_RGB[k]/RGB_TS_max)
                        else:
                            axs[0].imshow(D_RGB[k]/np.max(D_RGB[k]))
                    # IR patch
                    if k in D_IR.keys():
                        if TS_norm:
                            axs[1].imshow(D_IR[k]/IR_TS_max)
                        else:
                            axs[1].imshow(D_IR[k]/np.max(D_IR[k]))
                plt.show()

            else:
                print('Wrong orient value')
        else:
            # At least 2 elements
            # Depending on orient value:
            if orient=='H': #default
                # PLOTS patches
                fig, axs = plt.subplots(2, len(Ls), figsize=(6*len(Ls),11) )

                # Loop on union keys
                for i,k in enumerate(Ls):
                    # Name & disable axis
                    axs[0,i].set_title(k, fontsize=50)
                    axs[0,i].axis(False)
                    axs[1,i].axis(False)

                    # RGB patch
                    if k in D_RGB.keys():
                        if TS_norm:
                            axs[0,i].imshow(D_RGB[k]/RGB_TS_max)
                        else:
                            axs[0,i].imshow(D_RGB[k]/np.max(D_RGB[k]))
                    # IR patch
                    if k in D_IR.keys():
                        if TS_norm:
                            axs[1,i].imshow(D_IR[k]/IR_TS_max)
                        else:
                            axs[1,i].imshow(D_IR[k]/np.max(D_IR[k]))
                plt.show()

            elif orient=='V':
                # PLOTS patches
                fig, axs = plt.subplots(len(Ls), 2, figsize=(11,6*len(Ls)) )

                # Loop on union keys
                for i,k in enumerate(Ls):
                    # Name & disable axis
                    axs[i,0].set_title(k, fontsize=35)
                    axs[i,0].axis(False)
                    axs[i,1].axis(False)

                    # RGB patch
                    if k in D_RGB.keys():
                        if TS_norm:
                            axs[i,0].imshow(D_RGB[k]/RGB_TS_max)
                        else:
                            axs[i,0].imshow(D_RGB[k]/np.max(D_RGB[k]))
                    # IR patch
                    if k in D_IR.keys():
                        if TS_norm:
                            axs[i,1].imshow(D_IR[k]/IR_TS_max)
                        else:
                            axs[i,1].imshow(D_IR[k]/np.max(D_IR[k]))
                plt.show()

            else:
                print('Wrong orient value')

            if fig_p:
                # Name
                name = '_'.join([orient, 'plot', str(gbifid)])
                if TS_norm:
                    name+= '_TSnorm'
                name += '.png'
                print("Saved at provided path wh name:", name)
                # Effective save
                fig.savefig(os.path.join(fig_p, name), bbox_inches='tight')

            # closes fig
            plt.close()
    else:
        print("No available data here for occurrence", gbifid)