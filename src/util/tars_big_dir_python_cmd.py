# TOOK AROUND 4h --> Put at least 6h in wt to be sure
# CONFIG:
#SBATCH -A oyu@gpu
#SBATCH --cpus-per-task=2
#SBATCH --qos=qos_gpu-t4
#SBATCH --partition=gpu_p2
#
# OOM memory error with 8G for mem-per-cpu, try more
# %%
from subprocess import Popen
import os

# %%
dir_to_tar = "/gpfsscratch/rech/oyu/commun/Sentinel-2/patches/16bits_png/final_dataset_by_gbifid/"
dest_dir   = "/gpfswork/rech/oyu/uzk84jj/Orchids_project/data/datasets"
tar_p      = os.path.join(dest_dir, "final_dataset_by_gbifid_2cmdline2.tar")

cmd        = "python3 -m tarfile -c " + tar_p +" "+ dir_to_tar

# Launches process
proc = Popen(cmd, shell=True)
proc.wait()

# Print
print("Process terminated successfully since this point was reached!")