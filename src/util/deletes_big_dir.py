# TOOK AROUND 4h --> Put at least 6h in wt to be sure
# CONFIG:
#SBATCH -A oyu@gpu
#SBATCH --cpus-per-task=2
#SBATCH --qos=qos_gpu-t4
#SBATCH --partition=gpu_p2
#
# %%
from subprocess import Popen

# UNTARRED DIR
dir = "/gpfsscratch/rech/oyu/commun/Sentinel-2/patches/16bits_png/final_dataset"

# cmd
cmd1 = "rm -r " + dir           # deletes dir

# Launches process
proc = Popen(cmd1, shell=True)
proc.wait()

# Print
print("Process terminated successfully since this point was reached!")