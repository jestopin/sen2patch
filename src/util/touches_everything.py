# TOOK AROUND 4h --> Put at least 6h in wt to be sure
# CONFIG:
#SBATCH -A oyu@gpu
#SBATCH --cpus-per-task=2
#SBATCH --qos=qos_gpu-t4
#SBATCH --partition=gpu_p2
#
# %%
from subprocess import Popen


# %%
touch_p = "/gpfsscratch/rech/oyu/commun/Sentinel-2"
cmd     = "find "+touch_p+" -type f -exec touch {} +"


# Launches process
proc = Popen(cmd, shell=True)
proc.wait()

# Print
print("Process terminated successfully as this point was reached!")