# TOOK AROUND 4h --> Put at least 6h in wt to be sure
# CONFIG:
#SBATCH -A oyu@gpu
#SBATCH --cpus-per-task=2
#SBATCH --qos=qos_gpu-t4
#SBATCH --partition=gpu_p2
#
# %%

import os
import tarfile

# %%
dest_dir   = "/gpfsscratch/rech/oyu/commun/Sentinel-2/patches/16bits_png/extract_verif/final_dataset_verif2"
data_dir   = "/gpfswork/rech/oyu/uzk84jj/Orchids_project/data/datasets"
tar_p      = os.path.join(data_dir, "final_dataset2cmdline.tar")

tar = tarfile.open(tar_p)
tar.extractall(path = dest_dir)
tar.close()

# Print
print("Process terminated successfully since this point was reached!")