# TOOK AROUND 4h --> Put at least 6h in wt to be sure
# CONFIG:
#SBATCH -A oyu@gpu
#SBATCH --cpus-per-task=2
#SBATCH --qos=qos_gpu-t4
#SBATCH --partition=gpu_p2
#
# %%
from subprocess import Popen

# dir
    # ORIGINAL
# dir = "/gpfsscratch/rech/oyu/commun/Sentinel-2/patches/16bits_png/final_dataset"
    # UNTARRED DIR
dir = "/gpfsscratch/rech/oyu/commun/Sentinel-2/patches/16bits_png/duplicates_by_gbifid"
    # TEST
# dir = "/gpfsscratch/rech/oyu/uzk84jj/normalisation"

# cmd
cmd1 = "du -sh " + dir                     # Measure size in GB
cmd2 = "find " + dir + " -type f | wc -l"  # Count files (f) or directories (d)

# Launches process
proc = Popen(cmd1, shell=True)
proc.wait()
proc = Popen(cmd2, shell=True)
proc.wait()

# Print
print("Process terminated successfully since this point was reached!")