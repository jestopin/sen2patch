#!/usr/bin/env python
# coding: utf-8

# # API Query
# 
# - Queries sentinelAPI for a given set of tiles and parameters (especially a given **time period**)
# - Saves query result in **out_path** folder.
# 

# ## I. Downloads needed tiles from *Needed_tiles_selection* for a given time period

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
from sentinelsat import SentinelAPI
import os
import time
import random
import glob
import pandas as pd
import seaborn as sns
from datetime import date, timedelta
from calendar import monthrange


# ### User Inputs

# In[2]:


# Code was designed to work with queries spanning full months - Adaptations might be needed if other choice made
### INPUTS ###
# tiles_file_p = "../data/S2_tiles/sample300_tiles_set_default.npz"
tiles_file_p = "/gpfswork/rech/oyu/uzk84jj/Orchids_project/sen2patch/data/S2_tiles/missing_tiles.csv"
usr_choices_p= "/gpfswork/rech/oyu/uzk84jj/Orchids_project/sen2patch/user_choices.npz"
usr_choices  = np.load(usr_choices_p, allow_pickle=True)
dates        = list(usr_choices['dates'])
out_p        = "/gpfswork/rech/oyu/uzk84jj/Orchids_project/sen2patch/data/product_ids"
#############


# - Dates choices

# In[3]:


month_index          = 2
start_date, end_date = dates[month_index]
start_date, end_date


# ### API connection

# An account on https://scihub.copernicus.eu/ is needed to query the API. Account validation can take 'may take up to a week', normally 2/3 days.

# In[4]:


user, password = 'one000', 'visible_password!!!'
api = SentinelAPI(user, password, 'https://apihub.copernicus.eu/apihub')
api


# ### Targeted tiles

# In[5]:


if tiles_file_p[-3:]=="npz":
    data = np.load(tiles_file_p, allow_pickle=True)
    D    = data['D'].item()

    print("type D: ", type(D))
    print("len  D: ", len(D))
    print("item[0]    : ", list(D.items())[0])
    tiles      = list(D.keys())
elif tiles_file_p[-3:]=="csv":
    t = pd.read_csv(tiles_file_p)
    t.rename(columns={"Unnamed: 0": "tile"}, inplace=True)
    t.set_index('tile', inplace=True)
    tiles = list(t.index)


# In[6]:


len(tiles)


# ### Query arguments

# - 'producttype':
#     - 'S2MSI1C' corresponds to Top of Atmosphere products (without atmos. correction), globally available
#     - 'S2MSI2A' corresponds to Bottom of Atmosphere products (with atmos. correction), not globally available on this end-point at least
# - See https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-2/data-products and other docs for more information.

# In[7]:


query_kwargs = {
    'platformname': 'Sentinel-2',
    'producttype' : 'S2MSI1C',
    'date'        : (start_date, end_date) #,
    # 'ondemand'    : 'true'                        # All queried products are offline, don't know why...
}


# ### API query
# - Select all tiles within time window. Than, retain tiles with least cloud cover % for each possible (tile,orbit)
# - Results are put in an ordered dictionnary.

# In[ ]:


query_ID = np.random.randint(100000)
I = [0,5]
# List of targeted tiles
L_target = tiles[I[0]:I[1]]
# count products
c = 0
# tiles with no matching products
Lmiss = []

# All considered products
products = OrderedDict()

for i,tile in enumerate(L_target):
    print(i, tile)
    kw           = query_kwargs.copy()
    kw['tileid'] = tile
    pp           = api.query(**kw)
    # No tiles
    if len(pp)==0:
        Lmiss.append(tile)
    # At least one tile
    else:
        products.update(pp)
        

print("Nb of found tiles\t:", (len(L_target) - len(Lmiss)), " / ",len(L_target))
print(len(Lmiss), "tiles not found\t:", Lmiss)

# Saves result
np.savez(os.path.join(out_p, str(start_date)+'_'+str(end_date)+'_queryID'+str(query_ID)+'_before_selection_I-'+str(I[0])+'-'+str(I[1])+'.npz'),
         products = products)


# In[ ]:


# Conversion
products_df = api.to_dataframe(products).convert_dtypes()
print(products_df.shape)
products_df.head()


# In[ ]:


print(products_df.link[0])
print(products_df.link_alternative[0])
print(products_df.link_icon[0])
print(products_df.identifier[0])


# ### Less cloudy products selection per (tile, orbit) pair

# In[ ]:


# print(products_geodf.columns)
grouped = products_df.groupby(['tileid', 'relativeorbitnumber'], sort=False, group_keys=False)
dataset = grouped.apply(lambda x: x.sort_values(('cloudcoverpercentage'), ascending=True).head(1))


# In[ ]:


L = [(tile, orbit) for tile, orbit in zip(products_df.tileid, products_df.relativeorbitnumber)]
print("Total #(tile, orbit)\t:", len(L))
print("Unique #(tile, orbit)\t:", len(set(L)))
print("Dataset\t:", dataset.shape[0])


# ### Saves API query results

# In[ ]:


dataset_dic = dataset.to_dict(into=OrderedDict, orient='index')


# In[ ]:


print("SIZE avail. products\t: ", api.get_products_size(products), 'GB')
print("SIZE final selection\t: ", api.get_products_size(dataset_dic), 'GB')


# In[ ]:


out_file = os.path.join(out_p, str(start_date)+'_'+str(end_date)+'_queryID'+str(query_ID)+'.npz')
print("out_file:", out_file)

# Saves result
np.savez(out_file,
         products = dataset_dic,
         Lunfound = Lmiss)


# In[ ]:


list(dataset_dic.items())[15][0]


# In[ ]:


# Download test
# api.download(list(dataset_dic.items())[15][0])
# api.download_all(dataset_dic)


# In[ ]:




