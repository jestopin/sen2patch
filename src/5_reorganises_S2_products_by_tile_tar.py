#!/usr/bin/env python
# coding: utf-8

# # S2 data regorganisation.
# It was first intended to reduce the huge number of inodes on disk ($STORE).
#
# The chosen architecture in a main folder called */tiles* is:\
# ```
# tiles
# |
# |-tile_id
# |    |-tile_id_yyyy_mm
# |    |     |-S2A_MSIL1C_...
# |    |     |-S2B_MSIL1C_...
# |    |     |-...
# |    |-...
# |
# |-...
# ```
#
# - Each *tile_id* folder (there are 7563 of them for the Orchid study) will be then compress in *.tar* format.

# In[4]:


import numpy as np
from collections import OrderedDict
import os
import time
import random
import glob
import tarfile
import shutil

# ## Inputs

# In[10]:


### INPUTS ###
D_file_p      = "../data/S2_tiles/sample300_tiles_set_default.npz"
Lmonths_new_p = glob.glob("../data/products/by_month/*")             # List of targeted months to be added to tiles .tar files
tiles_p       = "../data/products/by_tile/"
#############


# In[5]:


data  = np.load(D_file_p, allow_pickle=True)
D     = data['D'].item()
tiles = list(D.keys())
#tiles = ['35SLV']

# ## II. For a single tile

# In[8]:

# Checks if tiles_p exists and creates it if needed
if not(os.path.exists(tiles_p)):
    os.mkdir(tiles_p)
    print("Folder created: ", tiles_p)

# ## III For ALL TILES

for tile1 in tiles:

# In[14]:


    # Matching .tar file path
    tar_p = tiles_p + tile1 + '.tar'
    # 'a' or 'a:' Open for appending with no compression. The file is created if it does not exist.
    tar = tarfile.open(tar_p, 'a')

    # List of already present tileid_yyyy_mm folders in tile .tar file
    Lmonths_tar = [l.split('/')[-1] for l in tar.getnames() if 'S2' not in l.split('/')[-1]] 									# .getmembers for more infos see doc
    print("Lmonths_tar  :", Lmonths_tar)

    # List of new months in down_p to be added to existing tile .tar file
    #L_months_p = sorted(glob.glob(down_p+"*queryID*"))
    print('Lmonths_new_p:', Lmonths_new_p)
    print()

    # For month in L_months_new_p
    for month_p in Lmonths_new_p:
        # Reads matching yyyy_mm date
        month_date = month_p.split('/')[-1].split('_')[0]
        tileid_yyyy_mm    = tile1 +'_'+ month_date.split('-')[0]+'_'+month_date.split('-')[1]
        print('Tested (tile/month):', tileid_yyyy_mm)

        # Checks if yyyy_mm already exists in tile .tar file
        # By default add new products only if yyyy_mm does not already exist
        if tileid_yyyy_mm not in Lmonths_tar:
            # Creates uncrompressed yyyy_mm folder
            tileid_yyyy_mm_p = tiles_p + tileid_yyyy_mm
            os.mkdir(tileid_yyyy_mm_p)
            print("\tFolder created:", '*'+tileid_yyyy_mm_p+'*')
            # Searches for matching products in matching month folder
            for prod in os.listdir(month_p):
                if prod[39:44] == tile1: # Or comp by accessing tile with gdal if needed only
                    # Moves them (/ creates eponym empty dir to begin) in yyyy_mm_p folder
                    # os.mkdir(tileid_yyyy_mm_p+'/'+prod)
                    shutil.move(month_p+'/'+prod, tileid_yyyy_mm_p+'/'+prod)
                    # shutil.copyfile(month_p+'/'+prod, tileid_yyyy_mm_p+'/'+prod)
                    print("\t\t...Working with file ", prod)

            # Adds yyyy_mm folder to tile .tar file with tar.add(name)
            tar.add(tileid_yyyy_mm_p, arcname=tileid_yyyy_mm)
            print('\t', tileid_yyyy_mm, "added to archive", tile1)
            # Deletes tileid_yyyy_mm folder
            shutil.rmtree(tileid_yyyy_mm_p)
        else:
            print('\t', tileid_yyyy_mm, "already exists in", tile1)


    # Close tile .tar file
    tar.close()
